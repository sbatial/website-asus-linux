+++
title = "Nvidia Dynamic Boost isn't working!"
+++

<del>As of version 510.60.02, NVIDIA still doesn't support dynamic boost on AMD CPUs. This is a known issue might get fixed in some future release. 
</del>

Since version 525.53, NVIDIA added official Dynamic Boost support for AMD laptop that uses Ryzen 6000 Series (or newer) CPU.

To enable it, follow the next steps:

1. Start nvidia-powerd.service

```bash
sudo systemctl start nvidia-powerd.service
```
If you don't want to start it manually each time, then:

```bash
sudo systemctl enable nvidia-powerd.service
```
<del>Notice: There is a known bug that nvidia-powerd process uses excessive CPU after you offload the prime run application. You can stop by:</del>

<del>sudo systemctl stop nvidia-powerd.service</del>

Update: version 530.41.03/525.105.17 and newer driver fixed this issue. Now you can enable nvidia-powered without worrying about high CPU usage.

2. Set your power profile to "Performance" Mode

You can do it in many ways. If you already installed asusctl, you can switch to it using:

```bash
asusctl profile -P Performance
```
or ROG Control Center (GUI) to set it.

Besides, you can enable it using your DE control panel (such as KDE/GNOME).

If you didn't see any option in DE panel, then you need to check if the "power-profiles-daemon" package was properly installed.

3. Test it

Using tools like "nvtop" and "mangohud", you can monitor your CPU and GPU power in realtime.

To check if the Dynamic boost works, you need to identify what is the MAX TGP your model support. Usually, it can be found on manufacturers' websites. 

Here are some power data collected from Zephyrus G15 (2022), which rated 120 watt MAX TGP.

Quiete Mode: 25 Watt (CPU) + 60 Watt (GPU)

Balanced Mode: 30 Watt (CPU) + 80 Watt (GPU)

Performance Mode: 25 Watt (CPU) + 115 Watt (GPU)