+++
title = "Using an external display has low FPS or is extremely laggy"
+++

This problem mostly appears when connect your external display to the dGPU in Hybrid mode (rendering screens on separate GPUs). There are various reasons that can cause this problem and we don't have a perfect solution for it yet. But here are some possible ways to solve it.

1. Use X11 instead of Wayland.

2. If your model supports video via USB-C and one of the USB-C ports takes the signal from iGPU, then buy a USB-C to HDMI/DP cable or dongle. this way, you can avoid rendering two screens on separate GPUs. (tested on 2022 Zephyrus G15)