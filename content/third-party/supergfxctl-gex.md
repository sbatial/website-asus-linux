+++
title = "supergfxctl-gex"
weight = 1
+++

supergfxctl-gex is a **frontend** for supergfxctl that was born inside the asus-linux.org community.

To install it, visit [supergfxctl-gex on the GNOME extension site](https://extensions.gnome.org/extension/5344/supergfxctl-gex/) and install it from there. You'll need to install the `gnome-shell-extension` package for your distro and browser plugin to make the one-click install work properly!

![supergfxctl-gex screenshot](/images/third-party/supergfxctl-gex.png)

> I'm currently searching maintainers. If you are into JavaScript / TypeScript and want to help out a little, please give me a call (marco@laux.wtf)

You can view the source code at [https://gitlab.com/asus-linux/supergfxctl-gex](https://gitlab.com/asus-linux/supergfxctl-gex)